<?php

namespace Drupal\Tests\field_ipaddress\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the ip field functionality.
 *
 * @group field_ipaddress
 */
class IpAddressFieldTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ipaddress',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The node type to use in tests.
   *
   * @var \Drupal\node\NodeTypeInterface
   */
  protected $nodeType;

  /**
   * The field name to use in tests.
   *
   * @var string
   */
  protected $fieldName = 'field_test_ip';

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();
    $this->nodeType = $this->drupalCreateContentType(['type' => 'test_type']);

    $this->drupalLogin($this->createUser([
      'create test_type content',
    ]));

    // Add a duration field to test content type.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => 'node',
      'type' => 'ipaddress',
      'settings' => [],
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $this->nodeType->id(),
      'settings' => [
        'allow_range' => TRUE,
        'allow_family' => 4,
        'ip4_range' => '',
        'ip6_range' => '',
      ],
    ]);
    $field->save();

    // Configure the displays to make sure field is shown on node form and view.
    $form = \Drupal::configFactory()
      ->getEditable('core.entity_form_display.node.' . $this->nodeType->id() . '.default');
    $form->set('content.' . $this->fieldName . '.type', 'ipaddress_default')
      ->set('content.' . $this->fieldName . '.settings', [])
      ->set('content.' . $this->fieldName . '.third_party_settings', [])
      ->set('content.' . $this->fieldName . '.weight', 0)
      ->save();
    // Configure the widget to make sure field in shown on node form.
    $display = \Drupal::configFactory()
      ->getEditable('core.entity_view_display.node.' . $this->nodeType->id() . '.default');
    $display->set('content.' . $this->fieldName . '.type', 'ipaddress_default')
      ->set('content.' . $this->fieldName . '.label', 'hidden')
      ->set('content.' . $this->fieldName . '.settings', [])
      ->set('content.' . $this->fieldName . '.third_party_settings', [])
      ->set('content.' . $this->fieldName . '.weight', 0)
      ->save();
  }

  /**
   * Tests the ipaddress field with different settings and values.
   *
   * @param array $field_settings
   *   The ip field instance settings.
   * @param string $value
   *   IP address value.
   * @param string $expected
   *   The expected output for the field.
   * @param string $error
   *   The expected error message.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ElementTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Core\Entity\EntityStorageException
   *
   * @dataProvider ipAddressFieldDataProvider
   */
  public function testIpAddressField(array $field_settings, string $value, string $expected = '', string $error = '') {
    // Apply field settings.
    $field = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $field->set('settings', $field_settings)->save();

    // Create new node of test type.
    $this->drupalGet('node/add/' . $this->nodeType->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->submitForm([
      'title[0][value]' => 'Test node',
      $this->fieldName . '[0][value]' => $value,
    ], 'Save');

    if ($error) {
      $this->assertSession()->pageTextContains($error);
    }
    elseif ($expected) {
      $this->assertSession()->addressMatches('/^\/node\/\d$/');
      $this->assertSession()->elementTextContains('css', 'article', $expected);
    }
  }

  /**
   * Data provider for testIpAddressField.
   */
  public function ipAddressFieldDataProvider() {
    return [
      // Test correct single value with default settings.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 4,
          'ip4_range' => '',
          'ip6_range' => '',
        ],
        '107.127.42.16',
        '107.127.42.16',
      ],
      // Test correct range value with default settings.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 4,
          'ip4_range' => '',
          'ip6_range' => '',
        ],
        '107.127.42.16-117.137.62.56',
        '107.127.42.16-117.137.62.56',
      ],
      // Test incorrect value with default settings.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 4,
          'ip4_range' => '',
          'ip6_range' => '',
        ],
        '107.127.42.316',
        '',
        'Invalid IP or range.',
      ],
      // Test incorrect range value with default settings.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 4,
          'ip4_range' => '',
          'ip6_range' => '',
        ],
        '107.127.42.16-310.124.62.56',
        '',
        'Invalid IP or range.',
      ],
      // Test range value when range values are not allowed.
      [
        [
          'allow_range' => FALSE,
          'allow_family' => 4,
          'ip4_range' => '',
          'ip6_range' => '',
        ],
        '107.127.42.16-117.137.62.56',
        '',
        'Ranges not allowed, single IP only.',
      ],
      // Test correct value with v4 range specified.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 4,
          'ip4_range' => '1.1.1.1-100.100.100.100',
          'ip6_range' => '',
        ],
        '17.27.42.16',
        '17.27.42.16',
      ],
      // Test correct range value with v4 range specified.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 4,
          'ip4_range' => '1.1.1.1-100.100.100.100',
          'ip6_range' => '',
        ],
        '7.17.12.16-37.57.72.56',
        '7.17.12.16-37.57.72.56',
      ],
      // Test incorrect value with v4 range specified.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 4,
          'ip4_range' => '1.1.1.1-100.100.100.100',
          'ip6_range' => '',
        ],
        '107.127.42.16',
        '',
        'IP must be within the range 1.1.1.1-100.100.100.100',
      ],
      // Test incorrect range value with v4 range specified.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 4,
          'ip4_range' => '1.1.1.1-100.100.100.100',
          'ip6_range' => '',
        ],
        '87.17.12.16-127.99.72.86',
        '',
        'IP must be within the range 1.1.1.1-100.100.100.100',
      ],
      // Test correct value when v6 allowed.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 6,
          'ip4_range' => '',
          'ip6_range' => '',
        ],
        '2001:db8:3333:4444:5555:6666:7777:8888',
        '2001:db8:3333:4444:5555:6666:7777:8888',
      ],
      // Test correct range value when v6 allowed.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 6,
          'ip4_range' => '',
          'ip6_range' => '',
        ],
        '2001:db8:3333:4444:5555:6666:7777:8888-2001:db8:4444:5555:6666:7777:8888:9999',
        '2001:db8:3333:4444:5555:6666:7777:8888-2001:db8:4444:5555:6666:7777:8888:9999',
      ],
      // Test incorrect value when v6 allowed.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 6,
          'ip4_range' => '',
          'ip6_range' => '',
        ],
        '2001:db8:3333:4444:5555:6666:7777:ZZZZ',
        '',
        'Invalid IP or range.',
      ],
      // Test incorrect range value when v6 allowed.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 6,
          'ip4_range' => '',
          'ip6_range' => '',
        ],
        '2001:db8:3333:4444:5555:6666:7777:ZZZZ-2001:db8:3333:4444:5555:6666:8888:ZZZZ',
        '',
        'Invalid IP or range.',
      ],
      // Test correct value with v6 range specified.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 6,
          'ip4_range' => '',
          'ip6_range' => '0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
        ],
        '999:999:999:999:999:999:999:999',
        '999:999:999:999:999:999:999:999',
      ],
      // Test correct range value with v6 range specified.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 6,
          'ip4_range' => '',
          'ip6_range' => '0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
        ],
        '888:888:888:888:888:888:888:888-999:999:999:999:999:999:999:999',
        '888:888:888:888:888:888:888:888-999:999:999:999:999:999:999:999',
      ],
      // Test incorrect value with v6 range specified.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 6,
          'ip4_range' => '',
          'ip6_range' => '0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
        ],
        '1001:1001:1001:1001:1001:1001:1001:1001',
        '',
        'IP must be within the range 0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
      ],
      // Test incorrect range value with v6 range specified.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 6,
          'ip4_range' => '',
          'ip6_range' => '0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
        ],
        '999:999:999:999:999:999:999:999-1001:1001:1001:1001:1001:1001:1001:1001',
        '',
        'IP must be within the range 0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
      ],
      // Test correct v4 address when both allowed and have ranges.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 10,
          'ip4_range' => '1.1.1.1-100.100.100.100',
          'ip6_range' => '0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
        ],
        '12.34.56.78',
        '12.34.56.78',
      ],
      // Test incorrect v4 address when both allowed and have ranges.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 10,
          'ip4_range' => '1.1.1.1-100.100.100.100',
          'ip6_range' => '0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
        ],
        '121.34.56.78',
        '',
        'IP must be within the range 1.1.1.1-100.100.100.100',
      ],
      // Test correct v6 address when both allowed and have ranges.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 10,
          'ip4_range' => '1.1.1.1-100.100.100.100',
          'ip6_range' => '0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
        ],
        '123:123:123:123:123:123:123:123',
        '123:123:123:123:123:123:123:123',
      ],
      // Test incorrect v6 address when both allowed and have ranges.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 10,
          'ip4_range' => '1.1.1.1-100.100.100.100',
          'ip6_range' => '0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
        ],
        '1234:123:123:123:123:123:123:123',
        '',
        'IP must be within the range 0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
      ],
      // Test invalid data when both allowed and have ranges.
      [
        [
          'allow_range' => TRUE,
          'allow_family' => 10,
          'ip4_range' => '1.1.1.1-100.100.100.100',
          'ip6_range' => '0001:0001:0001:0001:0001:0001:0001:0001-1000:1000:1000:1000:1000:1000:1000:1000',
        ],
        'INVALID DATA',
        '',
        'Invalid IP or range.',
      ],
    ];
  }

}
