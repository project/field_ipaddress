<?php

/**
 * @file
 * Provides views data for the field_ipaddress module.
 */

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data().
 */
function field_ipaddress_field_views_data(FieldStorageConfigInterface $field_storage) {
  $data = views_field_default_views_data($field_storage);
  $name = $field_storage->getName();
  foreach ($data as &$table_data) {
    $table_data[$name]['filter'] = [
      'id' => 'ip_address',
    ];
    $table_data[$name]['sort'] = [
      'id' => 'ip_address',
    ];
  }

  return $data;
}
