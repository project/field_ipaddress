<?php

namespace Drupal\field_ipaddress\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Basic sort handler for ip address fields.
 *
 * @ViewsSort("ip_address")
 */
class IpAddressSort extends SortPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // This would make sure that ranges are sorted properly. If the sort is ASC,
    // we want to sort by the start of the range, if it's DESC, we want to sort
    // by the end of the range.
    $this->realField = $this->field . ($this->options['order'] === 'ASC' ? '_ip_start' : '_ip_end');
    parent::query();
  }

}
