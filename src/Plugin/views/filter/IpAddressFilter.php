<?php

namespace Drupal\field_ipaddress\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\field_ipaddress\IpAddress;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Basic filter handler for ip address fields.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("ip_address")
 */
class IpAddressFilter extends FilterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function operatorOptions() {
    return [
      '=' => $this->t('Is equal to'),
      '!=' => $this->t('Is not equal to'),
      '<' => $this->t('Is less than'),
      '<=' => $this->t('Is less than or equal to'),
      '>' => $this->t('Is greater than'),
      '>=' => $this->t('Is greater than or equal to'),
      'BETWEEN' => $this->t('Is between'),
      'NOT BETWEEN' => $this->t('Is not between'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();
    $start_col = $this->tableAlias . '.' . $this->field . '_ip_start';
    $end_col = $this->tableAlias . '.' . $this->field . '_ip_end';
    if (!empty($this->value)) {
      $value = is_array($this->value) ? reset($this->value) : $this->value;
      $value = new IpAddress($value);
      $args = [
        ':start' => inet_pton($value->start()),
        ':end' => inet_pton($value->end()),
      ];
      if (in_array($this->operator, ['BETWEEN', 'NOT BETWEEN'])) {
        // If we have a BETWEEN or NOT BETWEEN operator we need to make sure
        // ip ranges are within the filter range and vice versa.
        $snippet = $this->operator === 'BETWEEN'
          ? "$start_col >= :start AND $end_col <= :end"
          : "$start_col > :end OR $end_col < :start";
        $this->query->addWhereExpression($this->options['group'], $snippet, $args);
      }
      else {
        // If we have a single operator we need to make sure that both
        // ends of ip range is equal/more/less/etc. than the filter range.
        $op = $this->operator;
        $this->query->addWhereExpression($this->options['group'], "$start_col $op :start AND $end_col $op :end", $args);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    $form['value']['#type'] = 'textfield';
    $form['value']['#title'] = $this->t('IP address');
    $form['value']['#default_value'] = $this->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  protected function valueValidate($form, FormStateInterface $form_state) {
    parent::valueValidate($form, $form_state);
    $value = $form_state->getValue(['options', 'value']);
    try {
      new IpAddress($value);
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('options][value', $this->t('Invalid IP address or range.'));
    }
  }

}
